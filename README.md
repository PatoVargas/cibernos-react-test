# Cibernos Fifa World Cup

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm  run start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.
You may also see any lint errors in the console.

### `npm run test`

Launches the test and show project coverage\.

### `npm run lint:fix`

Launches the lint stage and in addition try to resolve the errors founded.

### `npm run prepare`

Init the husky pre-commit configuration.

## NPM packages used

- material-ui: to accelerate development
- husky: pre-commit validations
- moment: manipulation of dates
- react-router-dom: routes in application
- uuid: id generations
- commitlint/cli: commit format check
- eslint: static analisis of code
- prettier: format the code
- jest: testing project components
- react-world-flags: to get countries flags
- prop-types: type check in react components

## Project considerations

- The project is structured on the basis of atomic design, where atoms are omitted since they will be obtained from ui-material.

- For storage World Cup Matches, the project use session storage. If you delete it, the project comeback to the initial state.

- By definition, the project needs 100% coverage in all criteria.

- Before push the project, husky run automatically lint, tests and commit check.

- Some components was created from material-ui examples to accelerate development, but they were adapted to the needs of this project.

- Basic gitlab pipeline was configured in .gitlab-ci.yml

- For demo visit https://cibernos-frontend-test-patovargas.vercel.app/, this was deployed by pipeline.
