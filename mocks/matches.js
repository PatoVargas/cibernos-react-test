export const fakeMatches = [
  {
    id: '49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd',
    homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
    awayTeam: { score: 4, name: 'Chile', abbr: 'CL' },
    date: '2023-01-08T17:47:00.557Z'
  },
  {
    id: '4fbcc628-6405-44b5-9f39-b43a9bbd9d03',
    homeTeam: { score: 0, name: 'Angola', abbr: 'AO' },
    awayTeam: { score: 0, name: 'Albania', abbr: 'AL' },
    date: '2023-01-08T17:55:17.060Z'
  }
];

export const emptyMatches = [];
