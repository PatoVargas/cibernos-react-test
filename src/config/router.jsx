import React from 'react';
import { createBrowserRouter } from 'react-router-dom';

import Scoreboard from '../components/pages';
import StartGame from '../components/pages/startGame';
import Summary from '../components/pages/summary';
import applicationPaths from './paths';

const { scoreboard, summary, startGame } = applicationPaths;

const router = createBrowserRouter([
  {
    path: scoreboard,
    element: <Scoreboard />
  },
  {
    path: summary,
    element: <Summary />
  },
  {
    path: startGame,
    element: <StartGame />
  }
]);

export default router;
