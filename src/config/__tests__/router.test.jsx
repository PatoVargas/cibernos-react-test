import router from '../router';

describe('router', () => {
  test('return router', () => {
    const isHomeRoute = router.routes.find((route) => route.path === '/');
    expect(isHomeRoute).not.toBeUndefined();
  });
});
