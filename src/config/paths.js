const applicationPaths = {
  scoreboard: '/',
  startGame: '/start-game',
  summary: '/summary'
};

export default applicationPaths;
