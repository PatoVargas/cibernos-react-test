import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import Scoreboard from '..';

function WrrapedScoreboardPage() {
  return (
    <BrowserRouter>
      <Scoreboard />
    </BrowserRouter>
  );
}

describe('Scoreboard Page', () => {
  test('Render page', () => {
    render(<WrrapedScoreboardPage />);
    const title = screen.getByText('This is the scoreboard for live matches');
    expect(title).toBeInTheDocument();
  });
});
