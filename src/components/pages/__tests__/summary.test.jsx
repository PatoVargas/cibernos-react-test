import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import Summary from '../summary';

function WrrapedSummaryPage() {
  return (
    <BrowserRouter>
      <Summary />
    </BrowserRouter>
  );
}

describe('Summary Page', () => {
  test('Render page', () => {
    render(<WrrapedSummaryPage />);
    const title = screen.getByText('This is the summary board');
    expect(title).toBeInTheDocument();
  });
});
