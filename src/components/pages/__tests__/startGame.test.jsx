import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import StartGame from '../startGame';

function WrrapedStartGamePage() {
  return (
    <BrowserRouter>
      <StartGame />
    </BrowserRouter>
  );
}

describe('StartGame Page', () => {
  test('Render page', () => {
    render(<WrrapedStartGamePage />);
    const title = screen.getByText('Select home an away team, for start match');
    expect(title).toBeInTheDocument();
  });
});
