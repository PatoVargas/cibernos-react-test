import React, { useState } from 'react';
import storageKeys from '../../constants/storageKeys';
import { getMatches } from '../../utils/matches';
import HistoricalResults from '../organisms/HistoricalResults';
import MasterTemplate from '../templates/MasterTemplate';

const { historicalMatches } = storageKeys;

function Summary() {
  const [matches] = useState(getMatches(historicalMatches));

  return (
    <MasterTemplate>
      <HistoricalResults matches={matches} />
    </MasterTemplate>
  );
}

export default Summary;
