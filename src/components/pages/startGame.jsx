import React from 'react';
import StartGameForm from '../organisms/StartGameForm';

import MasterTemplate from '../templates/MasterTemplate';

function StartGame() {
  return (
    <MasterTemplate>
      <StartGameForm />
    </MasterTemplate>
  );
}

export default StartGame;
