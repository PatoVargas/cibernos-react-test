import React, { useState } from 'react';
import storageKeys from '../../constants/storageKeys';
import { getMatches } from '../../utils/matches';
import LiveResults from '../organisms/LiveResults';
import MasterTemplate from '../templates/MasterTemplate';

const { currentMatches } = storageKeys;

function Scoreboard() {
  const [matches, setMatches] = useState(getMatches(currentMatches));

  return (
    <MasterTemplate>
      <LiveResults matches={matches} setMatches={setMatches} />
    </MasterTemplate>
  );
}

export default Scoreboard;
