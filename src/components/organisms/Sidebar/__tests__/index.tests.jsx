import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import Sidebar from '..';

function WrrapedSidebar() {
  return (
    <BrowserRouter>
      <Sidebar />
    </BrowserRouter>
  );
}

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate
}));

describe('Sidebar', () => {
  test('Render page', () => {
    render(<WrrapedSidebar />);
    const scoreboardText = screen.getByText('Scoreboard');
    expect(scoreboardText).toBeInTheDocument();
    const startGameText = screen.getByText('Start Game');
    expect(startGameText).toBeInTheDocument();
    const summaryText = screen.getByText('Summary');
    expect(summaryText).toBeInTheDocument();
    const cleanText = screen.getByText('Clean Cup');
    expect(cleanText).toBeInTheDocument();
  });

  test('onClick clean cup and cancel delete', async () => {
    render(<WrrapedSidebar />);
    const cleanButton = screen.getByTestId('delete-cup-option');
    fireEvent.click(cleanButton);
    const modalTitle = screen.getByText('Are you sure?');
    const modalSubtitle = screen.getByText('This is a permanent action');
    expect(modalTitle).toBeInTheDocument();
    expect(modalSubtitle).toBeInTheDocument();
    const cancelButtonModal = screen.getByTestId('confirmation-modal-cancel');
    fireEvent.click(cancelButtonModal);
    await waitFor(() => {
      const modalTitleSecondTime = screen.queryByText('Are you sure?');
      expect(modalTitleSecondTime).not.toBeInTheDocument();
    });
  });

  test('onClick clean cup and confirm delete', async () => {
    render(<WrrapedSidebar />);
    const cleanButton = screen.getByTestId('delete-cup-option');
    fireEvent.click(cleanButton);
    const confirmButtonModal = screen.getByTestId('confirmation-modal-confirm');
    fireEvent.click(confirmButtonModal);
    expect(mockedUsedNavigate).toBeCalledWith('/');
  });

  test('onClick element with route', async () => {
    render(<WrrapedSidebar />);
    const summaryButton = screen.getByTestId('summary-option');
    fireEvent.click(summaryButton);
    expect(mockedUsedNavigate).toBeCalledWith('/summary');
  });
});
