import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import Toolbar from '@mui/material/Toolbar';
import ScoreboardIcon from '@mui/icons-material/ContentPaste';
import SportsIcon from '@mui/icons-material/Sports';
import SportsSoccerIcon from '@mui/icons-material/SportsSoccer';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import applicationPaths from '../../../config/paths';
import ConfirmationModal from '../../molecules/ConfirmationModal';
import { deleteWorldCupMatches } from '../../../utils/storage';

const { scoreboard, summary, startGame } = applicationPaths;

function Sidebar() {
  const navigate = useNavigate();
  const [openDeleteModal, setOpenDeleteModal] = useState(false);
  const menuOptions = [
    {
      route: scoreboard,
      name: 'Scoreboard',
      icon: <ScoreboardIcon />,
      id: 'scoreboard-option'
    },
    {
      route: startGame,
      name: 'Start Game',
      icon: <SportsIcon />,
      id: 'start-game-option'
    },
    {
      route: summary,
      name: 'Summary',
      icon: <SportsSoccerIcon />,
      id: 'summary-option'
    },
    {
      name: 'Clean Cup',
      icon: <DeleteForeverIcon />,
      onClick: () => setOpenDeleteModal(true),
      id: 'delete-cup-option'
    }
  ];

  const onConfirmDeleteMatches = () => {
    deleteWorldCupMatches();
    navigate(scoreboard);
  };

  const onCancelDeleteMatches = () => setOpenDeleteModal(false);

  return (
    <div>
      <Toolbar />
      <Divider />
      <List>
        {menuOptions.map(({ id, route, name, icon, onClick }) => (
          <div key={name}>
            <ListItem>
              <ListItemButton
                id={id}
                data-testid={id}
                onClick={route ? () => navigate(route) : onClick}
              >
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={name} />
              </ListItemButton>
            </ListItem>
            <Divider />
          </div>
        ))}
      </List>
      <ConfirmationModal
        onConfirm={onConfirmDeleteMatches}
        open={openDeleteModal}
        onCancel={onCancelDeleteMatches}
      />
    </div>
  );
}

export default Sidebar;
