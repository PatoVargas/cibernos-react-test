import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  within
} from '@testing-library/react';
import StartGameForm from '..';
import { fakeMatches } from '../../../../../mocks/matches';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate
}));

function WrappedStartGameForm(props) {
  return (
    <BrowserRouter>
      <StartGameForm {...props} />
    </BrowserRouter>
  );
}

const localStorageMock = (() => {
  let store = {};

  return {
    getItem(key) {
      return store[key] || null;
    },
    setItem(key, value) {
      store[key] = value.toString();
    },
    removeItem(key) {
      delete store[key];
    },
    clear() {
      store = {};
    }
  };
})();

Object.defineProperty(window, 'sessionStorage', {
  value: localStorageMock
});

describe('StartGameForm', () => {
  beforeEach(() => {
    window.sessionStorage.clear();
    jest.restoreAllMocks();
  });

  test('Render and create match with success', async () => {
    render(<WrappedStartGameForm />);
    const homeTeaminput = screen.getByTestId('country-home-team-input');
    expect(homeTeaminput).toBeInTheDocument();
    const arrowHomeTeam =
      within(homeTeaminput).getByTestId('ArrowDropDownIcon');
    fireEvent.click(arrowHomeTeam);
    const albaniaText = screen.queryByText('Albania');
    await waitFor(() => {
      expect(albaniaText).toBeInTheDocument();
    });
    fireEvent.click(albaniaText);
    const awayTeaminput = screen.getByTestId('country-away-team-input');
    expect(awayTeaminput).toBeInTheDocument();
    const arrowAwayTeam =
      within(awayTeaminput).getByTestId('ArrowDropDownIcon');
    fireEvent.click(arrowAwayTeam);
    const algeriaText = screen.queryByText('Algeria');
    await waitFor(() => {
      expect(algeriaText).toBeInTheDocument();
    });
    fireEvent.click(algeriaText);
    const createButton = screen.getByTestId('create-match-button');
    expect(createButton).toBeInTheDocument();
    expect(createButton).not.toBeDisabled();
    fireEvent.click(createButton);
    const modalText = screen.queryByText('Match created successfully!');
    expect(modalText).toBeInTheDocument();
    fireEvent.keyDown(modalText, {
      key: 'Escape',
      code: 'Escape',
      keyCode: 27,
      charCode: 27
    });
    expect(mockedUsedNavigate).toBeCalledWith('/');
  });

  test('Render and show error, because are same teams', async () => {
    render(<WrappedStartGameForm />);
    const homeTeaminput = screen.getByTestId('country-home-team-input');
    expect(homeTeaminput).toBeInTheDocument();
    const arrowHomeTeam =
      within(homeTeaminput).getByTestId('ArrowDropDownIcon');
    fireEvent.click(arrowHomeTeam);
    const firstTeam = screen.queryByText('Albania');
    await waitFor(() => {
      expect(firstTeam).toBeInTheDocument();
    });
    fireEvent.click(firstTeam);
    const awayTeaminput = screen.getByTestId('country-away-team-input');
    expect(awayTeaminput).toBeInTheDocument();
    const arrowAwayTeam =
      within(awayTeaminput).getByTestId('ArrowDropDownIcon');
    fireEvent.click(arrowAwayTeam);
    const secondTeam = screen.queryByText('Albania');
    await waitFor(() => {
      expect(secondTeam).toBeInTheDocument();
    });
    fireEvent.click(secondTeam);
    const createButton = screen.getByTestId('create-match-button');
    expect(createButton).toBeInTheDocument();
    expect(createButton).toBeDisabled();
    fireEvent.click(createButton);
    const errorText = screen.queryByText('Please, select differents teams');
    expect(errorText).toBeInTheDocument();
  });

  test('Render and show error, because one team is playing', async () => {
    render(<WrappedStartGameForm />);
    window.sessionStorage.setItem(
      'current-matches',
      JSON.stringify(fakeMatches)
    );

    const homeTeaminput = screen.getByTestId('country-home-team-input');
    expect(homeTeaminput).toBeInTheDocument();
    const arrowHomeTeam =
      within(homeTeaminput).getByTestId('ArrowDropDownIcon');
    fireEvent.click(arrowHomeTeam);
    const firstTeam = screen.queryByText('Andorra');
    await waitFor(() => {
      expect(firstTeam).toBeInTheDocument();
    });
    fireEvent.click(firstTeam);
    const awayTeaminput = screen.getByTestId('country-away-team-input');
    expect(awayTeaminput).toBeInTheDocument();
    const arrowAwayTeam =
      within(awayTeaminput).getByTestId('ArrowDropDownIcon');
    fireEvent.click(arrowAwayTeam);
    const secondTeam = screen.queryByText('Chile');
    await waitFor(() => {
      expect(secondTeam).toBeInTheDocument();
    });
    fireEvent.click(secondTeam);
    const createButton = screen.getByTestId('create-match-button');
    expect(createButton).toBeInTheDocument();
    expect(createButton).not.toBeDisabled();
    fireEvent.click(createButton);
    const errorText = screen.queryByText(
      'This match exists, please select another combination'
    );
    expect(errorText).toBeInTheDocument();
  });

  test('Render and select teams, but after delete its', async () => {
    render(<WrappedStartGameForm />);
    window.sessionStorage.setItem(
      'current-matches',
      JSON.stringify(fakeMatches)
    );

    const homeTeaminput = screen.getByTestId('country-home-team-input');
    expect(homeTeaminput).toBeInTheDocument();
    const arrowHomeTeam =
      within(homeTeaminput).getByTestId('ArrowDropDownIcon');
    fireEvent.click(arrowHomeTeam);
    const firstTeam = screen.queryByText('Andorra');
    await waitFor(() => {
      expect(firstTeam).toBeInTheDocument();
    });
    fireEvent.click(firstTeam);
    const homeContainerInput = screen.getByTestId('country-home-team');
    expect(homeContainerInput).toBeInTheDocument();
    fireEvent.mouseOver(firstTeam);
    const crossHomeTeam = within(homeContainerInput).getByTitle('Clear');
    expect(crossHomeTeam).toBeInTheDocument();
    fireEvent.click(crossHomeTeam);
    const awayTeaminput = screen.getByTestId('country-away-team-input');
    expect(awayTeaminput).toBeInTheDocument();
    const arrowAwayTeam =
      within(awayTeaminput).getByTestId('ArrowDropDownIcon');
    fireEvent.click(arrowAwayTeam);
    const secondTeam = screen.queryByText('Chile');
    await waitFor(() => {
      expect(secondTeam).toBeInTheDocument();
    });
    fireEvent.click(secondTeam);
    const awayContainerInput = screen.getByTestId('country-away-team');
    expect(awayContainerInput).toBeInTheDocument();
    fireEvent.mouseOver(secondTeam);
    const crossAwayTeam = within(awayContainerInput).getByTitle('Clear');
    expect(crossAwayTeam).toBeInTheDocument();
    fireEvent.click(crossAwayTeam);
    const createButton = screen.getByTestId('create-match-button');
    expect(createButton).toBeInTheDocument();
    expect(createButton).toBeDisabled();
  });
});
