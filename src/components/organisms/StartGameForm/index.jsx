import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import Alert from '@mui/material/Alert';
import Typography from '@mui/material/Typography';

import storageKeys from '../../../constants/storageKeys';
import { setInSessionStorage } from '../../../utils/storage';
import {
  checkIfMatchCanBeCreated,
  getMatches,
  startMatch
} from '../../../utils/matches';
import CountryDropdown from '../../molecules/CountryDropdown';
import ModalSuccess from '../../molecules/ModalSuccess';
import applicationPaths from '../../../config/paths';

const { currentMatches } = storageKeys;

function StartGameForm() {
  const navigate = useNavigate();
  const [homeTeam, setHomeTeam] = useState(undefined);
  const [awayTeam, setAwayTeam] = useState(undefined);
  const [openModal, setOpenModal] = useState(false);
  const [disableButton, setDisableButton] = useState(true);
  const [errorMessage, setErrorMessage] = useState('');
  const onChangeHomeTeam = (_event, value) => setHomeTeam(value || {});

  const onChangeAwayTeam = (_event, value) => setAwayTeam(value || {});

  useEffect(() => {
    if (homeTeam && awayTeam && homeTeam.abbr === awayTeam.abbr) {
      setErrorMessage('Please, select differents teams');
      setDisableButton(true);
    } else if (!homeTeam || !awayTeam || !homeTeam.abbr || !awayTeam.abbr) {
      setDisableButton(true);
    } else {
      setDisableButton(false);
      setErrorMessage('');
    }
  }, [homeTeam, awayTeam]);

  const onClickButton = () => {
    setErrorMessage('');
    const newMatch = startMatch(homeTeam, awayTeam);
    const matches = getMatches(currentMatches);
    const canBePlayed = checkIfMatchCanBeCreated(matches, newMatch);
    if (!canBePlayed.status) {
      setErrorMessage(canBePlayed.message);
    } else {
      setOpenModal(true);
      setInSessionStorage(currentMatches, [...matches, newMatch]);
    }
  };

  const onCloseModal = () => navigate(applicationPaths.scoreboard);

  return (
    <Box sx={{ width: '100%' }}>
      <Typography variant="h5" gutterBottom align="center" marginBottom="20px">
        Select home an away team, for start match
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12} md={6}>
          <CountryDropdown
            id="country-home-team"
            onChange={onChangeHomeTeam}
            label="Choose home country"
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <CountryDropdown
            id="country-away-team"
            onChange={onChangeAwayTeam}
            label="Choose away country"
          />
        </Grid>
      </Grid>
      <Stack
        spacing={2}
        direction="row"
        justifyContent="center"
        marginTop="20px"
      >
        <Button
          id="create-match-button"
          data-testid="create-match-button"
          variant="outlined"
          size="large"
          disabled={disableButton}
          onClick={onClickButton}
        >
          Start Match
        </Button>
      </Stack>
      {errorMessage && (
        <Stack
          spacing={2}
          direction="row"
          justifyContent="center"
          marginTop="20px"
        >
          <Alert severity="error">{errorMessage}</Alert>
        </Stack>
      )}
      <ModalSuccess
        open={openModal}
        setOpen={setOpenModal}
        onClose={onCloseModal}
        message="Match created successfully!"
      />
    </Box>
  );
}

export default StartGameForm;
