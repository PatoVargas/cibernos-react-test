import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { render, screen } from '@testing-library/react';
import HistoricalResults from '..';
import { emptyMatches, fakeMatches } from '../../../../../mocks/matches';

function WrappedHistoricalResults(props) {
  return (
    <BrowserRouter>
      <HistoricalResults {...props} />
    </BrowserRouter>
  );
}

describe('HistoricalResults', () => {
  test('Render with matches', () => {
    render(<WrappedHistoricalResults matches={fakeMatches} />);
    const chileText = screen.getByText('Chile');
    expect(chileText).toBeInTheDocument();
    const score = screen.getByText('0 - 4');
    expect(score).toBeInTheDocument();
  });

  test('Render without matches', () => {
    render(<WrappedHistoricalResults matches={emptyMatches} />);
    const title = screen.getByText(
      'Empty results! add matches to start Fifa World Cup'
    );
    expect(title).toBeInTheDocument();
  });
});
