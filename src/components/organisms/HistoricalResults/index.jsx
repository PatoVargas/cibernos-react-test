import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@mui/material/Typography';

import EmptyResults from '../../molecules/EmptyResults';
import HistoricalResultsTable from '../../molecules/HistoricalResultsTable';

function HistoricalResults({ matches }) {
  return (
    <>
      <Typography variant="h5" gutterBottom align="center" marginBottom="20px">
        This is the summary board
      </Typography>
      {matches.length > 0 ? (
        <HistoricalResultsTable matches={matches} />
      ) : (
        <EmptyResults />
      )}
    </>
  );
}

HistoricalResults.propTypes = {
  matches: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      homeTeam: PropTypes.shape({
        abbr: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        score: PropTypes.number.isRequired
      }).isRequired,
      awayTeam: PropTypes.shape({
        abbr: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        score: PropTypes.number.isRequired
      }).isRequired
    })
  ).isRequired
};

export default HistoricalResults;
