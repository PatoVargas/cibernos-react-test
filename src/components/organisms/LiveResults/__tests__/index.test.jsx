import React, { useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import LiveResults from '..';
import { fakeMatches } from '../../../../../mocks/matches';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate
}));

function WrappedLiveResults() {
  const [matches, setMatches] = useState([fakeMatches[0]]);
  return (
    <BrowserRouter>
      <LiveResults matches={matches} setMatches={setMatches} />
    </BrowserRouter>
  );
}

const localStorageMock = (() => {
  let store = {};

  return {
    getItem(key) {
      return store[key] || null;
    },
    setItem(key, value) {
      store[key] = value.toString();
    },
    removeItem(key) {
      delete store[key];
    },
    clear() {
      store = {};
    }
  };
})();

Object.defineProperty(window, 'sessionStorage', {
  value: localStorageMock
});

describe('StartGameForm', () => {
  beforeEach(() => {
    window.sessionStorage.clear();
    jest.restoreAllMocks();
  });

  test('Render, finish matches, and render empty state', () => {
    render(<WrappedLiveResults />);
    const title = screen.queryByText('This is the scoreboard for live matches');
    expect(title).toBeInTheDocument();
    const finishButton = screen.getByTestId(
      'finish-button-49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd'
    );
    fireEvent.click(finishButton);
    const deleteConfirmation = screen.queryByText('Are you sure to delete?');
    expect(deleteConfirmation).toBeInTheDocument();
    const confirmDelete = screen.getByTestId(
      'confirm-finish-49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd'
    );
    fireEvent.click(confirmDelete);
    const emptyStateText = screen.queryByText(
      'Empty results! add matches to start Fifa World Cup'
    );
    expect(emptyStateText).toBeInTheDocument();
    const createMatchButton = screen.getByTestId(
      'create-match-button-when-empty'
    );
    fireEvent.click(createMatchButton);
    expect(mockedUsedNavigate).toBeCalledWith('/start-game');
  });

  test('Render and edit match', () => {
    render(<WrappedLiveResults />);
    const title = screen.queryByText('This is the scoreboard for live matches');
    expect(title).toBeInTheDocument();
    const editButton = screen.getByTestId(
      'edit-button-match-49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd'
    );
    fireEvent.click(editButton);
    const homeScore = screen.getByTestId(
      'home-score-49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd-input'
    );
    fireEvent.change(homeScore, { target: { value: '2' } });
    const awayScore = screen.getByTestId(
      'away-score-49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd-input'
    );
    fireEvent.change(awayScore, { target: { value: '2' } });
    const confirmButton = screen.getByTestId(
      'confirm-edit-49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd'
    );
    fireEvent.click(confirmButton);
    const emptyStateText = screen.queryByText('Confirm edit match');
    expect(emptyStateText).not.toBeInTheDocument();
  });
});
