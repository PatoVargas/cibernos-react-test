import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

import LiveMatchSummary from '../../molecules/LiveMatchSummary';
import EmptyResults from '../../molecules/EmptyResults';

function LiveResults({ matches, setMatches }) {
  return (
    <>
      <Typography variant="h5" gutterBottom align="center" marginBottom="20px">
        This is the scoreboard for live matches
      </Typography>
      {matches.length > 0 ? (
        <Grid container spacing={2} justifyContent="center">
          {matches.map((match) => (
            <Grid
              item
              xs={12}
              sm={12}
              md={6}
              lg={4}
              xl={3}
              key={`live-result-${match.id}`}
            >
              <LiveMatchSummary match={match} setMatches={setMatches} />
            </Grid>
          ))}
        </Grid>
      ) : (
        <EmptyResults />
      )}
    </>
  );
}

LiveResults.propTypes = {
  matches: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      homeTeam: PropTypes.shape({
        abbr: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        score: PropTypes.number.isRequired
      }).isRequired,
      awayTeam: PropTypes.shape({
        abbr: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        score: PropTypes.number.isRequired
      }).isRequired
    })
  ).isRequired,
  setMatches: PropTypes.func.isRequired
};

export default LiveResults;
