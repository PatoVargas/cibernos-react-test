import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import MasterTemplate from '..';

const mockedUsedNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedUsedNavigate
}));

function WrappedMasterTemplate() {
  return (
    <BrowserRouter>
      <MasterTemplate>
        <div>Children</div>
      </MasterTemplate>
    </BrowserRouter>
  );
}

describe('MasterTemplate', () => {
  test('Render in mobile version, and click icon', () => {
    render(<WrappedMasterTemplate />);
    window.innerWidth = 375;
    window.innerHeight = 844;
    window.dispatchEvent(new Event('resize'));
    const drawerButton = screen.getByTestId('drawer-icon-button');
    expect(drawerButton).toBeInTheDocument();
    fireEvent.click(drawerButton);
  });
});
