import React from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';
import PropTypes from 'prop-types';

import CountryOption from '../CountryOption';
import countries from '../../../constants/countries';

function CountryDropdown({ id, label, onChange }) {
  const getOptionLabel = (option) => option.name;

  return (
    <Autocomplete
      id={id}
      data-testid={id}
      onChange={onChange}
      options={countries}
      autoHighlight
      getOptionLabel={getOptionLabel}
      renderOption={(props, option) => (
        <CountryOption
          key={`${option.abbr}-${id}`}
          option={option}
          {...props}
        />
      )}
      renderInput={(params) => (
        <TextField
          {...params}
          data-testid={`${id}-input`}
          label={label}
          inputProps={{
            ...params.inputProps,
            autoComplete: 'off'
          }}
        />
      )}
    />
  );
}

CountryDropdown.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default CountryDropdown;
