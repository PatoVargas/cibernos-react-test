import React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

function ConfirmationModal({ open, onCancel, onConfirm }) {
  return (
    <Dialog open={open} onClose={onCancel}>
      <DialogTitle>Are you sure?</DialogTitle>
      <DialogContent>
        <DialogContentText>This is a permanent action</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          data-testid="confirmation-modal-cancel"
          id="confirmation-modal-cancel"
          onClick={onCancel}
        >
          Cancel
        </Button>
        <Button
          data-testid="confirmation-modal-confirm"
          id="confirmation-modal-confirm"
          onClick={onConfirm}
          autoFocus
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
}

ConfirmationModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired
};
export default ConfirmationModal;
