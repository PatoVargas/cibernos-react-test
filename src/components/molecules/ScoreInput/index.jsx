import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@mui/material/TextField';

function ScoreInput({ id, value, label, onChange }) {
  return (
    <TextField
      id={id}
      data-testid={id}
      value={value}
      sx={{ width: '80px' }}
      type="number"
      label={label}
      InputProps={{
        inputProps: { min: 0, step: 1, 'data-testid': `${id}-input` }
      }}
      onChange={onChange}
    />
  );
}

ScoreInput.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
};

export default ScoreInput;
