import React from 'react';
import Box from '@mui/material/Box';
import Flag from 'react-world-flags';
import PropTypes from 'prop-types';

function CountryOption(props) {
  const { option } = props;
  return (
    <Box
      component="li"
      id={option.abbr}
      sx={{ '& > img': { mr: 2, flexShrink: 0 } }}
      {...props}
    >
      <Flag code={option.abbr} height="12" widht="15" />
      {option.name}
    </Box>
  );
}

CountryOption.propTypes = {
  option: PropTypes.shape({
    abbr: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired
};

export default CountryOption;
