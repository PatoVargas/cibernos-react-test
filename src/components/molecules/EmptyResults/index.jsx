import React from 'react';
import { useNavigate } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';

import applicationPaths from '../../../config/paths';
import EmptyFutbolImage from '../../../assets/images/empty-futbol.jpeg';

function EmptyResults() {
  const navigate = useNavigate();

  const onClickButton = () => navigate(applicationPaths.startGame);

  return (
    <Grid container justifyContent="center">
      <Grid item xs={12}>
        <Typography
          variant="h5"
          gutterBottom
          align="center"
          marginBottom="20px"
        >
          Empty results! add matches to start Fifa World Cup
        </Typography>
      </Grid>
      <Grid item xs={12} textAlign="center">
        <img
          src={EmptyFutbolImage}
          alt="empty-stadium"
          loading="lazy"
          style={{ maxWidth: '80%' }}
        />
      </Grid>
      <Grid item xs={12} textAlign="center">
        <Button
          id="create-match-button-when-empty"
          data-testid="create-match-button-when-empty"
          variant="outlined"
          size="large"
          onClick={onClickButton}
        >
          Start Match
        </Button>
      </Grid>
    </Grid>
  );
}

export default EmptyResults;
