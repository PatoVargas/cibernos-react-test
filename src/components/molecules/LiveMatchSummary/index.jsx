import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Flag from 'react-world-flags';
import Alert from '@mui/material/Alert';

import FutbolImage from '../../../assets/images/futbol-stadium.jpeg';
import storageKeys from '../../../constants/storageKeys';
import {
  finishMatchAndReturnCurrentMatches,
  getMatches,
  updateMatchAndReturnCurrentMatches
} from '../../../utils/matches';
import ScoreInput from '../ScoreInput';

const { currentMatches } = storageKeys;

function LiveMatchSummary({ match, setMatches }) {
  const { homeTeam, awayTeam } = match;
  const [showFinishAlert, setShowFinishAlert] = useState(false);
  const [showEditAlert, setShowEditAlert] = useState(false);
  const [newHomeScore, setNewHomeScore] = useState(match.homeTeam.score);
  const [newAwayScore, setNewAwayScore] = useState(match.awayTeam.score);

  const onClickFinish = () => {
    setShowEditAlert(false);
    setShowFinishAlert(!showFinishAlert);
  };

  const onClickEdit = () => {
    setShowEditAlert(!showEditAlert);
    setShowFinishAlert(false);
  };

  const onConfirmFinish = () => {
    const matches = getMatches(currentMatches);
    const updatedMatches = finishMatchAndReturnCurrentMatches(matches, match);
    setMatches(updatedMatches);
    setShowFinishAlert(false);
  };

  const onChangeScore = (e, type) => {
    const { value } = e.target;
    if (type === 'home') setNewHomeScore(+value);
    else {
      setNewAwayScore(+value);
    }
  };

  const onConfirmEdit = () => {
    const matches = getMatches(currentMatches);
    const updatedMatch = {
      ...match,
      homeTeam: { ...match.homeTeam, score: newHomeScore },
      awayTeam: { ...match.awayTeam, score: newAwayScore }
    };
    const updatedMatches = updateMatchAndReturnCurrentMatches(
      matches,
      updatedMatch
    );
    setMatches(updatedMatches);
    setShowEditAlert(false);
  };

  return (
    <Card sx={{ maxWidth: 345, margin: 'auto' }}>
      <CardMedia component="img" height="140" image={FutbolImage} />
      <CardContent>
        <Typography
          variant="button"
          component="div"
          textAlign="center"
          marginBottom="10px"
        >
          {homeTeam.name} - {awayTeam.name}
        </Typography>
        <Typography
          gutterBottom
          variant="h5"
          component="div"
          textAlign="center"
          display="flex"
          justifyContent="center"
          border="1px solid black"
          padding="5px"
          sx={{ background: '#dcd8d8' }}
        >
          <Flag
            code={homeTeam.abbr}
            height="30"
            style={{ marginRight: '10px' }}
          />
          {homeTeam.score} - {awayTeam.score}{' '}
          <Flag
            code={awayTeam.abbr}
            height="30"
            style={{ marginLeft: '10px' }}
          />
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          size="small"
          onClick={onClickEdit}
          id={`edit-button-match-${match.id}`}
          data-testid={`edit-button-match-${match.id}`}
        >
          {showEditAlert ? 'Cancel Edit Match' : 'Edit Match'}
        </Button>
        <Button
          size="small"
          onClick={onClickFinish}
          id={`finish-button-${match.id}`}
          data-testid={`finish-button-${match.id}`}
        >
          {showFinishAlert ? 'Cancel Finish Match' : 'Finish Match'}
        </Button>
      </CardActions>
      {showFinishAlert && (
        <Alert
          severity="error"
          action={
            <Button
              color="inherit"
              size="small"
              onClick={onConfirmFinish}
              id={`confirm-finish-${match.id}`}
              data-testid={`confirm-finish-${match.id}`}
            >
              Confirm
            </Button>
          }
        >
          Are you sure to delete?
        </Alert>
      )}
      {showEditAlert && (
        <Alert
          severity="success"
          action={
            <Button
              color="inherit"
              size="small"
              onClick={onConfirmEdit}
              id={`confirm-edit-${match.id}`}
              data-testid={`confirm-edit-${match.id}`}
              sx={{ margin: 'auto' }}
            >
              Confirm
            </Button>
          }
        >
          <ScoreInput
            id={`home-score-${match.id}`}
            value={newHomeScore}
            label="Home"
            onChange={(e) => onChangeScore(e, 'home')}
          />{' '}
          <ScoreInput
            id={`away-score-${match.id}`}
            value={newAwayScore}
            label="Away"
            onChange={(e) => onChangeScore(e, 'away')}
          />{' '}
        </Alert>
      )}
    </Card>
  );
}

LiveMatchSummary.propTypes = {
  match: PropTypes.shape({
    id: PropTypes.string.isRequired,
    homeTeam: PropTypes.shape({
      abbr: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      score: PropTypes.number.isRequired
    }).isRequired,
    awayTeam: PropTypes.shape({
      abbr: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      score: PropTypes.number.isRequired
    }).isRequired
  }).isRequired,
  setMatches: PropTypes.func.isRequired
};

export default LiveMatchSummary;
