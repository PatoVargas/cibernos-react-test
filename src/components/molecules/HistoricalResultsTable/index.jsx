import * as React from 'react';
import PropTypes from 'prop-types';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Flag from 'react-world-flags';
import { orderHistoricalMatches } from '../../../utils/matches';
import formatDate from '../../../utils/dates';
import {
  awayFlag,
  containerAwayTeam,
  containerHomeTeam,
  firstColumnInRow,
  homeFlag
} from './styles';

function HistoricalResultsTable({ matches }) {
  const orderMatches = orderHistoricalMatches(matches);
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell align="center">Match</TableCell>
            <TableCell align="center">Result</TableCell>
            <TableCell align="center">Date</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orderMatches.map(
            ({
              id,
              homeTeam: { name: homeName, abbr: homeAbbr, score: homeScore },
              awayTeam: { name: awayName, abbr: awayAbbr, score: awayScore },
              date
            }) => (
              <TableRow
                key={`historical-result-${id}`}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell scope="row" align="center" style={firstColumnInRow}>
                  <Grid style={containerHomeTeam}>
                    <Flag code={homeAbbr} height="25" style={homeFlag} />
                    <Grid item display={{ xs: 'none', md: 'block' }}>
                      {homeName}
                    </Grid>
                  </Grid>
                  -
                  <Grid style={containerAwayTeam}>
                    <Grid item display={{ xs: 'none', md: 'block' }}>
                      {awayName}
                    </Grid>
                    <Flag code={awayAbbr} height="25" style={awayFlag} />
                  </Grid>
                </TableCell>
                <TableCell align="center">
                  {homeScore} - {awayScore}
                </TableCell>
                <TableCell align="center">{formatDate(date)}</TableCell>
              </TableRow>
            )
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

HistoricalResultsTable.propTypes = {
  matches: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      homeTeam: PropTypes.shape({
        abbr: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        score: PropTypes.number.isRequired
      }).isRequired,
      awayTeam: PropTypes.shape({
        abbr: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        score: PropTypes.number.isRequired
      }).isRequired
    })
  ).isRequired
};

export default HistoricalResultsTable;
