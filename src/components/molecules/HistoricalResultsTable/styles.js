export const firstColumnInRow = { display: 'flex', justifyContent: 'center' };

export const containerHomeTeam = {
  display: 'flex',
  verticalAlign: 'middle',
  marginRight: '10px'
};

export const containerAwayTeam = {
  display: 'flex',
  verticalAlign: 'middle',
  marginLeft: '10px'
};

export const homeFlag = { marginRight: '10px' };

export const awayFlag = { marginLeft: '10px' };
