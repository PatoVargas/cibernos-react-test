import * as React from 'react';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Alert from '@mui/material/Alert';
import PropTypes from 'prop-types';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  p: 4
};

function ModalSuccess({ open, message, setOpen, onClose }) {
  const handleClose = () => {
    setOpen(false);
    onClose();
  };

  return (
    <div>
      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <Alert severity="success">{message}</Alert>
        </Box>
      </Modal>
    </div>
  );
}

ModalSuccess.propTypes = {
  open: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  setOpen: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default ModalSuccess;
