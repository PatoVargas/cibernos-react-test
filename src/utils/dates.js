import moment from 'moment';

const formatDate = (date) => {
  return moment(date).format('DD-MM-YY HH:mm');
};

export default formatDate;
