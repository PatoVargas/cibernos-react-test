import { compareHistoricalMatchsByScore } from '../../matches';

describe('compareHistoricalMatchsByScore', () => {
  test('Return -1 because firstScore > secondScore', () => {
    const firstMatch = {
      homeTeam: { score: 5, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    };
    const secondMatch = {
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    };
    const results = compareHistoricalMatchsByScore(firstMatch, secondMatch);
    expect(results).toBe(-1);
  });

  test('Return -1 because firstScore date > secondScore date', () => {
    const firstMatch = {
      date: '2023-01-10T20:05:39.574Z',
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    };
    const secondMatch = {
      date: '2023-01-08T20:05:39.574Z',
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    };
    const results = compareHistoricalMatchsByScore(firstMatch, secondMatch);
    expect(results).toBe(-1);
  });

  test('Return 1 because firstScore date < secondScore date', () => {
    const firstMatch = {
      date: '2023-01-10T20:05:39.574Z',
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    };
    const secondMatch = {
      date: '2023-01-12T20:05:39.574Z',
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    };
    const results = compareHistoricalMatchsByScore(firstMatch, secondMatch);
    expect(results).toBe(1);
  });

  test('Return 0 because are equals', () => {
    const firstMatch = {
      date: '2023-01-10T20:05:39.574Z',
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    };
    const secondMatch = {
      date: '2023-01-10T20:05:39.574Z',
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    };
    const results = compareHistoricalMatchsByScore(firstMatch, secondMatch);
    expect(results).toBe(0);
  });
});
