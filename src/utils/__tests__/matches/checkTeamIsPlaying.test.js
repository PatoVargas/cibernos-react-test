import { checkTeamIsPlaying } from '../../matches';
import { fakeMatches } from '../../../../mocks/matches';

describe('checkTeamIsPlaying', () => {
  test('Return undefined. Match didt exist', () => {
    const results = checkTeamIsPlaying(fakeMatches, {
      homeTeam: {},
      awayTeam: {}
    });
    expect(results).toBeUndefined();
  });

  test('Return match, because team is play like home in another match', () => {
    const results = checkTeamIsPlaying(fakeMatches, {
      score: 0,
      name: 'Andorra',
      abbr: 'AD'
    });
    expect(results).toStrictEqual({
      awayTeam: { abbr: 'CL', name: 'Chile', score: 4 },
      date: '2023-01-08T17:47:00.557Z',
      homeTeam: { abbr: 'AD', name: 'Andorra', score: 0 },
      id: '49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd'
    });
  });
});
