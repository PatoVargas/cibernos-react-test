import { checkIfMatchExists } from '../../matches';
import { fakeMatches } from '../../../../mocks/matches';

describe('checkIfMatchExists', () => {
  test('Return undefined. Match didt exist', () => {
    const results = checkIfMatchExists(fakeMatches, {
      homeTeam: {},
      awayTeam: {}
    });
    expect(results).toBeUndefined();
  });

  test('Return match, because exist same match', () => {
    const results = checkIfMatchExists(fakeMatches, {
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    });
    expect(results).toStrictEqual({
      awayTeam: { abbr: 'CL', name: 'Chile', score: 4 },
      date: '2023-01-08T17:47:00.557Z',
      homeTeam: { abbr: 'AD', name: 'Andorra', score: 0 },
      id: '49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd'
    });
  });

  test('Return match, because exist same match but in other order', () => {
    const results = checkIfMatchExists(fakeMatches, {
      awayTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      homeTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    });
    expect(results).toStrictEqual({
      awayTeam: { abbr: 'CL', name: 'Chile', score: 4 },
      date: '2023-01-08T17:47:00.557Z',
      homeTeam: { abbr: 'AD', name: 'Andorra', score: 0 },
      id: '49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd'
    });
  });
});
