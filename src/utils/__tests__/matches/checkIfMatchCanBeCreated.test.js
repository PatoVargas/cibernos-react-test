import { checkIfMatchCanBeCreated } from '../../matches';
import { fakeMatches } from '../../../../mocks/matches';

describe('checkIfMatchCanBeCreated', () => {
  test('Return match can be created with success', () => {
    const results = checkIfMatchCanBeCreated(fakeMatches, {
      homeTeam: { abbr: 'KL' },
      awayTeam: { abbr: 'PR' }
    });
    expect(results).toStrictEqual({ status: true });
  });

  test('Return false, because same match exists', () => {
    const results = checkIfMatchCanBeCreated(fakeMatches, {
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    });
    expect(results).toStrictEqual({
      status: false,
      message: 'This match exists, please select another combination'
    });
  });

  test('Return false, because home team is playing', () => {
    const results = checkIfMatchCanBeCreated(fakeMatches, {
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Peru', abbr: 'PE' }
    });
    expect(results).toStrictEqual({
      status: false,
      message: 'Local Team (Andorra) is playing. Select other'
    });
  });

  test('Return false, because away team is playing', () => {
    const results = checkIfMatchCanBeCreated(fakeMatches, {
      homeTeam: { score: 0, name: 'Peru', abbr: 'PE' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' }
    });
    expect(results).toStrictEqual({
      status: false,
      message: 'Away Team (Chile) is playing. Select other'
    });
  });
});
