import { startMatch } from '../../matches';

describe('startMatch', () => {
  test('Return match can be created with success', () => {
    const results = startMatch({ abbr: 'KL' }, { abbr: 'PR' });
    expect(results.homeTeam.abbr).toBe('KL');
    expect(results.homeTeam.score).toBe(0);
    expect(results.awayTeam.score).toBe(0);
  });
});
