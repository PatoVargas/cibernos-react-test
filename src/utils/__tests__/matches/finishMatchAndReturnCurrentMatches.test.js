import { finishMatchAndReturnCurrentMatches } from '../../matches';
import { fakeMatches } from '../../../../mocks/matches';

describe('finishMatchAndReturnCurrentMatches', () => {
  test('Return updated matches', () => {
    const results = finishMatchAndReturnCurrentMatches(fakeMatches, {
      id: '49ba5e6e-dd1c-4d4a-b62a-a31bcec535fd',
      homeTeam: { score: 0, name: 'Andorra', abbr: 'AD' },
      awayTeam: { score: 4, name: 'Chile', abbr: 'CL' },
      date: '2023-01-08T17:47:00.557Z'
    });
    expect(results).toStrictEqual([fakeMatches[1]]);
  });
});
