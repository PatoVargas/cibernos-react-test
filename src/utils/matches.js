import { v4 as uuidv4 } from 'uuid';
import storageKeys from '../constants/storageKeys';
import { getFromSessionStorage, setInSessionStorage } from './storage';

export const getMatches = (key) => {
  return getFromSessionStorage(key) || [];
};

export const checkIfMatchExists = (matches, newMatch) => {
  return matches.find((match) => {
    return (
      (match.homeTeam.abbr === newMatch.homeTeam.abbr &&
        match.awayTeam.abbr === newMatch.awayTeam.abbr) ||
      (match.homeTeam.abbr === newMatch.awayTeam.abbr &&
        match.awayTeam.abbr === newMatch.homeTeam.abbr)
    );
  });
};

export const checkTeamIsPlaying = (matches, team) => {
  return matches.find((match) => {
    return (
      match.homeTeam.abbr === team.abbr || match.awayTeam.abbr === team.abbr
    );
  });
};

export const checkIfMatchCanBeCreated = (matches, newMatch) => {
  const { homeTeam, awayTeam } = newMatch;
  if (checkIfMatchExists(matches, newMatch)) {
    return {
      status: false,
      message: 'This match exists, please select another combination'
    };
  }
  if (checkTeamIsPlaying(matches, homeTeam)) {
    return {
      status: false,
      message: `Local Team (${homeTeam.name}) is playing. Select other`
    };
  }
  if (checkTeamIsPlaying(matches, awayTeam)) {
    return {
      status: false,
      message: `Away Team (${awayTeam.name}) is playing. Select other`
    };
  }
  return { status: true };
};

export const startMatch = (homeTeam, awayTeam) => {
  return {
    id: uuidv4(),
    homeTeam: { score: 0, ...homeTeam },
    awayTeam: { score: 0, ...awayTeam },
    date: new Date()
  };
};

export const finishMatchAndReturnCurrentMatches = (matches, endedMatch) => {
  const currentMatches = matches.filter((match) => match.id !== endedMatch.id);
  const historicalMatches = getMatches(storageKeys.historicalMatches);
  setInSessionStorage(storageKeys.currentMatches, currentMatches);
  setInSessionStorage(storageKeys.historicalMatches, [
    ...historicalMatches,
    endedMatch
  ]);
  return currentMatches;
};

export const updateMatchAndReturnCurrentMatches = (matches, updatedMatch) => {
  const currentMatches = matches.map((match) => {
    return match.id === updatedMatch.id ? updatedMatch : match;
  });
  setInSessionStorage(storageKeys.currentMatches, currentMatches);
  return currentMatches;
};

export const compareHistoricalMatchsByScore = (firstMatch, secondMatch) => {
  const firstScore = firstMatch.homeTeam.score + firstMatch.awayTeam.score;
  const secondScore = secondMatch.homeTeam.score + secondMatch.awayTeam.score;
  if (firstScore < secondScore) return 1;
  if (firstScore > secondScore) return -1;
  if (firstMatch.date < secondMatch.date) return 1;
  if (firstMatch.date > secondMatch.date) return -1;
  return 0;
};

export const orderHistoricalMatches = (matches) => {
  return matches.sort(compareHistoricalMatchsByScore);
};
