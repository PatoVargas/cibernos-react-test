import storageKeys from '../constants/storageKeys';

export const setInSessionStorage = (key, value) => {
  sessionStorage.setItem(key, JSON.stringify(value));
};

export const getFromSessionStorage = (key) => {
  return JSON.parse(sessionStorage.getItem(key));
};

export const deleteWorldCupMatches = () => {
  sessionStorage.removeItem(storageKeys.currentMatches);
  sessionStorage.removeItem(storageKeys.historicalMatches);
};
